# ℚTetris

Tetris bot in Python designed to achieve best performance and play infinitely.
Inspired by [Tetris AI - The (Near) Perfect Bot](https://codemyroad.wordpress.com/2013/04/14/tetris-ai-the-near-perfect-player/).

## Current performance

ℚTetris' implementation is highly optimized.

Currently, it can make about 50 moves per second (depends on computer and Python version).

## Technology

All code is written in Python.

This project uses mainly Tk (`tkinter`) for display, but components are highly reusable.
For example, there is very limited `pygame_display.py`, which was created in few minutes (requires `pygame` to run).

## No Q in ℚTetris

The first letter of ℚTetris is ℚ. Unfortunately, Git replaces this with Q, but ℚ should be used whenever possible.

## Installation

Download [latest source code](https://bitbucket.org/franz_wexler/qtetris/get/HEAD.zip).
Install Python version ≥ 3.5 with `tkinter`. Run `tk_display.py`. Enjoy.

## License

Umm... Do whatever you want as long as you credit me (Franz Wexler) and Code My Road.
